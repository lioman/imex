from urllib.parse import urlparse


def normalize_urls(url: str, site: str) -> str:
    """
    adds host and scheme to relative urls,
    returns absolute urls
    """
    if url.startswith("http"):
        return url
    parsed_site_url = urlparse(site)
    if url.startswith("//"):
        return f"{parsed_site_url.scheme}:{url}"
    return f"{site}{url}"
