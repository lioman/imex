import click
from blinker import signal

from scrape import load_file_into_list, load_site, extract_image_urls, download_images
from constants import NEW_URLS_SIGNAL, IMAGE_URLS_SIGNAL, HTML_RECEIVED_SIGNAL


def invoke_load_site(sender, **kwargs) -> None:
    """
    Glue function, called when new url signal is sent
    """
    if urls := kwargs.get("urls"):
        for url in urls:
            load_site(url)


def invoke_extract_image_urls(sender, **kwargs) -> None:
    """
    Glue function, called after html received signal is sent
    """
    html = kwargs.get("html")
    site_url = kwargs.get("url")
    if html and site_url:
        extract_image_urls(html=html, url=site_url)


def invoke_download(sender, **kwargs) -> None:
    """
    Glue function, called after new image urls signal is sent
    """
    if urls := kwargs.get("urls"):
        download_images(urls=urls)


def setup_signals() -> None:
    new_urls = signal(NEW_URLS_SIGNAL)
    html_received = signal(HTML_RECEIVED_SIGNAL)
    new_image_urls = signal(IMAGE_URLS_SIGNAL)
    new_urls.connect(invoke_load_site)
    html_received.connect(invoke_extract_image_urls)
    new_image_urls.connect(invoke_download)


@click.command()
@click.option("--file", help="File with urls one per line", prompt="File name")
def start_imex(file: str = None) -> None:
    """Loads a file with urls one per line that should be scraped."""
    setup_signals()
    load_file_into_list(file)


if __name__ == "__main__":
    start_imex()
