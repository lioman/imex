import logging
import os
from io import BytesIO
from pathlib import Path
from secrets import token_hex
from typing import List
from urllib.parse import urlparse

import requests
from PIL import Image
from blinker import signal
from bs4 import BeautifulSoup
from requests import RequestException

from constants import NEW_URLS_SIGNAL, HTML_RECEIVED_SIGNAL, IMAGE_URLS_SIGNAL
from helpers import normalize_urls

logger = logging.getLogger(__name__)


def load_file_into_list(file: str) -> List[str]:
    logger.debug("Load file")
    with open(file, "r") as f:
        urls = [line.replace("\n", "") for line in f if line != "\n"]
        new_urls = signal(NEW_URLS_SIGNAL)
        new_urls.send("anonymous", urls=urls)
        return urls


def load_site(url: str) -> [str, None]:
    logger.debug(f"Get HTML from {url}")
    try:
        response = requests.get(url)
        if not response.ok:
            logger.warning(f"Could not load {url}, Status {response.status_code}")
            return
        html = response.text
        try:
            html_received = signal(HTML_RECEIVED_SIGNAL)
            html_received.send("anonymous", html=html, url=url)
        except Exception as e:
            logger.error(f"{e}")
        return html
    except RequestException as e:
        logger.error(f"An Error occurred requesting: {url} - {e}")


def extract_image_urls(html: str, url: str) -> List[str]:
    soup = BeautifulSoup(html, features="html.parser")
    images = soup.find_all("img")
    if base_tag := soup.find("base"):
        base_url = base_tag.get("href")
        url = f"{url}{base_url}" if base_url.startswith("/") else base_url

    # TODO: load images from CSS defined like background: url(foo.jpg)
    urls = list(map(lambda image: normalize_urls(image.get("src"), url), images,))
    try:
        new_image_urls = signal(IMAGE_URLS_SIGNAL)
        new_image_urls.send("anonymous", urls=urls)
    except Exception as e:
        logger.error(f"{e}")
    return urls


def download_images(urls: List[str]) -> None:
    for url in urls:
        try:
            response = requests.get(url)
            if response.ok:
                i = Image.open(BytesIO(response.content))
                parsed_url = urlparse(url)
                # TODO: implement option to customize download directory
                filename = Path(os.path.basename(parsed_url.path))
                if filename.is_file():
                    filename = Path(filename).with_name(
                        f"{token_hex(4)}{filename.name}"
                    )
                i.save(filename)
        except Exception as e:
            logger.warning(f"{url} could not be fetched, or saved. {e}")
