import pytest

from helpers import normalize_urls


class TestNormalizeUrls:
    def test_absolute_urls_are_returned(self):
        assert "http://example.com" == normalize_urls(
            url="http://example.com", site="irrelevant"
        )

    def test_relative_urls_are_prepended_with_site(self):
        assert "http://example.com/image.png" == normalize_urls(
            url="/image.png", site="http://example.com"
        )

    def test_double_slash_are_prepended_with_http(self):
        assert "http://example.com/image.png" == normalize_urls(
            url="//example.com/image.png", site="http://example.com"
        )

    def test_double_slash_are_prepended_with_https(self):
        assert "https://example.com/image.png" == normalize_urls(
            url="//example.com/image.png", site="https://example.com"
        )
