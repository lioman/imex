import builtins
import logging
from unittest.mock import MagicMock

import pytest
import requests_mock
from bs4 import ResultSet, BeautifulSoup
from py._path.local import LocalPath
from requests import RequestException

from scrape import load_file_into_list, load_site, extract_image_urls, download_images


class TestLoadFileIntoList(object):
    def test_load_file(self, mocker, tmpdir):
        test_file: LocalPath = tmpdir.join("urls.txt")
        test_file.write(b"https://wikipedia.org\nhttps://apod.nasa.gov/apod/\n")
        file_name = test_file.strpath
        open_mock: MagicMock = mocker.spy(builtins, "open")
        assert load_file_into_list(file_name) == [
            "https://wikipedia.org",
            "https://apod.nasa.gov/apod/",
        ]
        open_mock.assert_called_once_with(file_name, "r")

    def test_load_file_empty_lines_are_removed(self, mocker, tmpdir):
        test_file: LocalPath = tmpdir.join("urls.txt")
        test_file.write(b"https://wikipedia.org\nhttps://apod.nasa.gov/apod/\n\n")
        file_name = test_file.strpath
        open_mock: MagicMock = mocker.spy(builtins, "open")
        assert load_file_into_list(file_name) == [
            "https://wikipedia.org",
            "https://apod.nasa.gov/apod/",
        ]
        open_mock.assert_called_once_with(file_name, "r")

    def test_non_existent_file_raises_exception(self, mocker):
        file_name = "this_path_does_not_exists"
        open_mock: MagicMock = mocker.spy(builtins, "open")
        with pytest.raises(FileNotFoundError):
            load_file_into_list(file_name)
            open_mock.assert_called_once_with(file_name, "r")


class TestLoadHTML(object):
    def test_load_html(self):
        with requests_mock.Mocker() as mock:
            test_url = "https://example.com"
            mock.get(test_url, text="<html></html>")
            assert load_site(test_url) == "<html></html>"

    def test_catch_exceptions(self, mocker):
        """
        If an request exception is thrown,
        it is logged and nothing is returned
        """
        with requests_mock.Mocker() as req_mock:
            test_url = "https://example.com"
            req_mock.get(test_url, exc=RequestException)
            logger = logging.getLogger("scrape")
            log_mock = mocker.patch.object(logger, "error")
            assert load_site(test_url) is None
            log_mock.assert_called_once()

    def test_handling_invalid_responses(self, mocker):
        """We need to handle all error responses"""
        with requests_mock.Mocker() as mock:
            test_url = "https://example.com"
            mock.get(test_url, text="", status_code=400)
            logger = logging.getLogger("scrape")
            log_mock = mocker.patch.object(logger, "warning")
            assert load_site(test_url) is None
            log_mock.assert_called_once()


class TestExtractImageUrls:
    def test_extract_urls(self, mocker):
        class SoupMock:
            def __init__(self, html_text, **extra):
                pass

            def find(self, tag):
                return

            def find_all(self, tag, **extra):
                return [
                    {"src": "https://example.com/image.png"},
                    {"src": "/relative.png"},
                ]

        mocker.patch("scrape.BeautifulSoup", side_effect=SoupMock)
        url = "https://example.com"
        html = "<html></html>"
        assert extract_image_urls(html, url) == [
            "https://example.com/image.png",
            "https://example.com/relative.png",
        ]

    def test_handle_base_tag(self, mocker):
        class SoupMock:
            def __init__(self, html_text, **extra):
                pass

            def find(self, tag):
                return {"href": "https://cdn.example.com"}

            def find_all(self, tag, **extra):
                return [
                    {"src": "https://example.com/image.png"},
                    {"src": "/relative.png"},
                ]

        mocker.patch("scrape.BeautifulSoup", side_effect=SoupMock)
        url = "https://example.com"
        html = "<html></html>"
        assert extract_image_urls(html, url) == [
            "https://example.com/image.png",
            "https://cdn.example.com/relative.png",
        ]


class TestDownloadImages:
    def test_handle_request_exception(self, mocker):
        """
        If an image could not be fetched, we just log it
        """
        with requests_mock.Mocker() as req_mock:
            req_mock.get("https://example.com/image.png", exc=RequestException)
            logger = logging.getLogger("scrape")
            log_mock = mocker.patch.object(logger, "warning")
            download_images(["https://example.com/image.png"])
            log_mock.assert_called_once()

    def test_download_images(self, mocker):
        class ImageMock:
            def __init__(self, t, **kwargs):
                pass

            def save(self, path):
                return

        with requests_mock.Mocker() as req_mock:
            req_mock.get("https://example.com/image.png", content=b"")
            image_open_mock = mocker.patch("PIL.Image.open", side_effect=ImageMock)
            # image_save_mock = mocker.patch("ImageMock")
            download_images(["https://example.com/image.png"])
            image_open_mock.assert_called_once()
            # image_save_mock.assert_called_once()  # save mock is missing

    def test_handling_of_duplicate_file_names(self, mocker):
        # mock request and image
        # mock Path.is(file)
        # assert filename gets random sting
        pass
